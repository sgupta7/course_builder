USE course_builder;
-- selects all section names and id for i_article
DELIMITER $$
CREATE PROCEDURE article_to_sections (IN i_article INT) BEGIN
	SELECT id, name
	FROM section
	WHERE article = i_article;
END $$
DELIMITER ;
-- selects all element id and data for i_section
-- data may be picture(blob), video(blob), or text(nvarchar(255))
DELIMITER $$
CREATE PROCEDURE section_to_elements (IN i_section INT) BEGIN
	SELECT a.id, text, picture, video
	FROM element a
	LEFT OUTER JOIN (
		SELECT id, data text
		FROM text
	) b ON a.id = b.id
	LEFT OUTER JOIN (
		SELECT id, data picture
		FROM picture
	) c ON a.id = c.id
	LEFT OUTER JOIN (
		SELECT id, data video
		FROM video
	) d ON a.id = d.id
	WHERE a.section = i_section;
END $$
DELIMITER ;
-- selects all question id and text for i_quiz
DELIMITER $$
CREATE PROCEDURE quiz_to_questions (IN i_quiz INT) BEGIN
	SELECT id, data FROM question WHERE quiz = i_quiz;
END $$
DELIMITER ;
-- select question text for i_question
DELIMITER $$
CREATE PROCEDURE question_text (IN i_question INT) BEGIN
	SELECT data FROM question WHERE id = i_question;
END $$
DELIMITER ;
-- get type of question
DELIMITER $$
CREATE PROCEDURE question_type (IN i_question INT) BEGIN
	SELECT COALESCE (
		(
			SELECT "blank"
			FROM fill_blank
			WHERE id = i_question
		),(
			SELECT "multi"
			FROM multi_choice
			WHERE id = i_question
		)
	) question_type;
END $$
DELIMITER ;
-- get the regular expression that accepts correct answers for i_question
DELIMITER $$
CREATE PROCEDURE question_to_regex (IN i_question INT) BEGIN
	SELECT regex FROM fill_blank WHERE id = i_question;
END $$
DELIMITER ;
-- validate a short answer
DELIMITER $$
CREATE PROCEDURE validate_blank_question
(IN i_answer VARCHAR(255), i_question INT) BEGIN
	SELECT i_answer LIKE regex valid FROM fill_blank WHERE id = i_question;
END $$
-- validate multiple choice answer
DELIMITER $$
CREATE PROCEDURE multi_choice_calidate_init() BEGIN
	CREATE TEMPORARY TABLE answer_set (id INT);
END $$
CREATE PROCEDURE multi_choice_validate_add(IN i_choice INT) BEGIN
	INSERT INTO answer_set
	(id) VALUES (i_choice);
END $$
CREATE PROCEDURE multi_choice_validate(IN i_question INT) BEGIN
	SELECT (
		SELECT GROUP_CONCAT(id ORDER BY id) FROM answer_set
	) IN (
		SELECT GROUP_CONCAT(choice ORDER BY choice)
		FROM correct_set
		WHERE question = i_question
		GROUP BY id
	) valid;
END $$
CREATE PROCEDURE multi_choice_validate_clean() BEGIN
	DROP TEMPORARY TABLE answer_set;
END $$
DELIMITER ;
-- get the list of choices for i_question
DELIMITER $$
CREATE PROCEDURE question_to_choices (IN i_question INT) BEGIN
	SELECT b.id, b.data
	FROM choice_set a
	JOIN choice b ON b.id = a.choice
	WHERE a.question = i_question;
END $$
DELIMITER ;
-- get a list of commonly used choices to reduce redundancy
DELIMITER $$
CREATE PROCEDURE select_common_choices () BEGIN
	SELECT b.id, b.data
	FROM common_choice a
	JOIN choice b ON a.id = b.id;
END $$
DELIMITER ;
-- get the set of correct answer combinations for i_question
DELIMITER $$
CREATE PROCEDURE question_to_correct_sets (IN i_question INT) BEGIN
	SELECT a.id set_id, b.id choice_id, b.data
	FROM correct_set a
	JOIN choice b ON b.id = a.choice
	WHERE a.question = i_question;
END $$
DELIMITER ;
-- get all articles for i_lesson
DELIMITER 8==D
CREATE PROCEDURE lesson_to_articles (IN i_lesson INT) BEGIN
	SELECT id, name
	FROM article
	WHERE lesson = i_lesson;
END 8==D
DELIMITER ;
-- get all quizzes for i_lesson
DELIMITER $$
CREATE PROCEDURE lesson_to_quiz (IN i_lesson INT) BEGIN
	SELECT id, name
	FROM quiz
	WHERE lesson = i_lesson;
END $$
DELIMITER ;
-- get all lessons for i_subject
DELIMITER $$
CREATE PROCEDURE subject_to_lessons (IN i_subject INT) BEGIN
	SELECT b.id, b.name
	FROM subject_set a
	JOIN lesson b ON a.lesson = b.id
	WHERE a.subject = i_subject;
END $$
DELIMITER ;
-- get a list of subjects for i_lesson
DELIMITER $$
CREATE PROCEDURE lesson_to_subjects (IN i_lesson INT) BEGIN
	SELECT b.id, b.name
	FROM subject_set a
	JOIN subject b ON a.subject = b.id
	WHERE a.lesson = i_lesson;
END $$
DELIMITER ;
-- get all subjects
DELIMITER $$
CREATE PROCEDURE select_subjects () BEGIN
	SELECT id, name
	FROM subject;
END $$
DELIMITER ;
-- get a list of users that took i_quiz
DELIMITER $$
CREATE PROCEDURE quiz_to_scores (IN i_quiz INT) BEGIN
	SELECT a.val, b.id, b.email
	FROM score a
	JOIN usr b ON a.usr = b.id
	WHERE a.quiz = i_quiz;
END $$
DELIMITER ;
-- get a list of quizzes taken by i_usr
DELIMITER $$
CREATE PROCEDURE user_to_scores (IN i_usr INT) BEGIN
	SELECT a.val, b.id, b.name
	FROM score a
	JOIN quiz b ON a.quiz = b.id
	WHERE a.usr = i_usr;
END $$
DELIMITER ;
-- get list of lessons written by i_producer
DELIMITER $$
CREATE PROCEDURE producer_to_lessons (IN i_producer INT) BEGIN
	SELECT id, name, admn
	FROM lesson
	WHERE producer = i_producer;
END $$
DELIMITER ;
-- get list of lessons published by i_admn
DELIMITER $$
CREATE PROCEDURE admin_to_lessons (IN i_admn INT) BEGIN
	SELECT id, name, producer
	FROM lesson
	WHERE admn = i_admn;
END $$
DELIMITER ;
-- get list of published lessons
DELIMITER $$
CREATE PROCEDURE select_published_lessons () BEGIN
	SELECT id, name, producer
	FROM lesson
	WHERE admn IS NOT NULL;
END $$
DELIMITER ;
-- get list of unpublished lessons
DELIMITER $$
CREATE PROCEDURE select_unpublished_lessons () BEGIN
	SELECT id, name, producer
	FROM lesson
	WHERE admn IS NULL;
END $$
DELIMITER ;
-- validate if user is producer
DELIMITER $$
CREATE PROCEDURE validate_producer (IN i_usr INT) BEGIN
	SELECT COUNT(*)
	FROM producer
	WHERE id = i_usr;
END $$
DELIMITER ;
-- validate if admin
DELIMITER $$
CREATE PROCEDURE validate_admin (IN i_usr INT) BEGIN
	SELECT COUNT(*)
	FROM admn
	WHERE id = i_usr;
END $$
DELIMITER ;
