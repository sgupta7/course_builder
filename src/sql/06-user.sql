USE course_builder;

DELIMITER $$

-- get user id, and admin and producer columns are 1 if user is one or both
CREATE PROCEDURE user_login
(IN i_pass VARCHAR(256), i_email VARCHAR(256)) BEGIN
	SELECT a.id id, b.id IS NOT NULL admn, c.id IS NOT NULL producer
	FROM usr a
	LEFT OUTER JOIN admn b
	ON a.id = b.id
	LEFT OUTER JOIN producer c
	ON a.id = c.id
	WHERE email = i_email
	AND hash = SHA1(CONCAT(i_pass, salt));
END $$

CREATE PROCEDURE user_create
(IN i_pass VARCHAR(256), i_email VARCHAR(256)) BEGIN
	INSERT INTO usr
	(hash, salt, email)
	VALUES
-- hard coded salt for now
	(SHA1(CONCAT(i_pass, 'deadsea')), 'deadsea', i_email);
END $$

DELIMITER ;
