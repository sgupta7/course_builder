The sql files should be executed in numerical order.
***

stored procedure list
=====================

select procedures
-----------------

returns all sections from a given article

columns: id, name

article_to_sections(INT article_id)
***

returns all elements from a given section

Either text, picture, or video will be not null

columns: id, text, picture, video

section_to_elements(INT section_id)
***

returns all questions from a given quiz

where "data" is the a text

columns: id, data

quiz_to_questions(INT quiz_id)
***

returns the text of a given question

where "data" is the question's text

columns: data

question_text(INT question_id)
***

returns the question's type

where "question_type" is either "blank" or "multi"

columns: question_type

question_type(INT question_id)
***

returns either 1 for valid or 0 for invalid

columns: valid

validate_blank_question(VARCHAR(255) answer, INT question_id)
***

creates a temporary table for validating multiple choice questions

multi_choice_validate_init()
***

add a choice to the temporary table created by multi_choice_validate_init()

multi_choice_validate_add(INT choice_id)
***

return valid = 1 if the answer is correct or valid = 0 otherwise

multi_choice_validate(INT question_id)
***

drop the temporary table

multi_choice_validate_clean()
***

returns the set of choices for a given question

where "data" is an option's text

columns: id, data

question_to_choices(INT question_id)
***

returns the regular expression that answers are validated with

columns: regex

question_to_regex(INT question_id)
***

returns a set of common choices, to reduce redundancy

where "data" is an option's text

select_common_choices()
***

returns a set of sets of the correct combinations
of choices for a given question

where "data" is an option's text

columns: set_id, option_id, data

question_to_correct_sets(INT question_id)
***

returns the articles for a given lesson

columns: id, name

lesson_to_articles(INT lesson_id)
***

returns the quizzes for a given lesson

columns: id, name

lesson_to_quiz(INT lesson_id)
***

returns the lessons under a given subject

columns: id, name

subject_to_lessons(INT subject_id)
***

returns the subjects that a given lesson is under

columns: id, name

lesson_to_subjects(INT lesson_id)
***

returns a list of subjects

columns: id, name

select_subjects()
***

returns the leaderboard of scores for a given quiz

where "val" is a score, "email" is a user's email,
and "id" is a user's id number

columns: val, id, email

quiz_to_scores(INT quiz_id)
***

returns the quiz scores of a given user

where "val" is a score, "id" is a quiz id number, and "name" is a quiz name

columns: val, id, name

user_to_scores(INT user_id)
***

returns all lessons written by a given producer

where "admn" is the id number of the admin that published the lesson, or null

columns: id, name, admn

producer_to_lessons(INT producer_id)
***

returns all lessons published by a given admin

where "producer" is the producer who wrote the lesson

columns: id, name, producer

admin_to_lessons(INT admin_id)
***

returns a list of published lessons; lessons with non-null "admn" columns

columns: id, name, producer

select_published_lessons()
***

returns a list of unpublished lessons; lessons with null "admn" columns

columns: id, name, producer

select_unpublished_lessons()
***

insertion procedures
--------------------

create a new lesson with the given name by the given producer

insert_lesson(NVARCHRA(25) name, INT producer_id)
***

when an admin publishes a lesson, associate it with the lesson

publish_lesson(INT lesson_id, INT admin_id)
***

add a quiz to a given lesson

insert_quiz(NVARCHAR(25) name, INT lesson_id)
***

add an article to the given lesson

insert_article(NVARCHAR(25) name, INT lesson_id)
***

associate a lesson with a subject

add_subject_to_lesson(INT lesson_id, INT subject_id)
***

add a section to an article

insert_section(NVARCHAR(25) name, INT article_id)
***

add a text element to a section

insert_text(NVARCHAR(255) text, INT section_id)
***

add a picture element to a section

where "data" is the binary data for a picture file

insert_picture(BLOB data, INT section_id)
***

add a video element to a section

where "data" is the binary data for a video file

insert_video(BLOB data, INT section_id)
***

add a fill-in-the-blank question to a quiz

where "text" is the question text and "regex" is a regular expression

insert_fill_blank(NVARCHAR text, VARCHAR(255) regex, INT quiz_id)
***

add a multiple-choice question to a quiz

where "text" is the question text

insert_multi_choice(NVARCHAR(255) text, INT quiz_id)
***

add a choice to a multiple-choice question

where "text" is the choice's text

add_choice_to_question(NVARCHAR(25) text, INT question_id)
***

add a choice to a set of correct choices for a given question

add_choice_to_correct_set(INT choice_id, INT question_id, INT set_id)
***

associate a user's score with a quiz

where "val" is the score value

add_score(INT user_id, INT quiz_id, INT val)
***

update a user's score

where "val" is a score value

update_score(INT user_id, INT quiz_id, INT val)
***

deletion procedures
-------------------

delete_user(INT id)
***

delete_producer(INT id)
***

delete_admin(INT id)
***

delete_lesson(INT id)
***

delete_quiz(INT id)
***

delete_article(INT id)
***

delete_section(INT id)
***

delete_element(INT id)
***

delete_question(INT id)
***

delete_choice(INT id)
***
