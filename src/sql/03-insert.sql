USE course_builder;
-- add a user
DELIMITER $$
CREATE PROCEDURE insert_user
(IN i_email VARCHAR(90), i_hash VARCHAR(20), i_salt VARCHAR(25)) BEGIN
	INSERT INTO usr
	(email, hash, salt) VALUES (i_email, i_hash, i_salt);
END $$
DELIMITER ;
-- promote i_usr to producer
DELIMITER $$
CREATE PROCEDURE insert_producer (IN i_usr INT) BEGIN
	INSERT INTO producer
	(id) VALUES (i_usr);
END $$
DELIMITER ;
-- promote i_usr to admin
DELIMITER $$
CREATE PROCEDURE insert_admin (IN i_usr INT) BEGIN
	INSERT INTO admn
	(id) VALUES (i_usr);
END $$
DELIMITER ;
-- i_producer writes a new lesson
DELIMITER $$
CREATE PROCEDURE insert_lesson (IN i_name NVARCHAR(25), i_producer INT) BEGIN
	INSERT INTO lesson
	(name, producer) VALUES (i_name, i_producer);
END $$
DELIMITER ;
-- i_admn publishes i_lesson
DELIMITER $$
CREATE PROCEDURE publish_lesson (IN i_lesson INT, i_admn INT) BEGIN
	UPDATE lesson
	SET admn = i_admn
	WHERE id = i_lesson;
END $$
DELIMITER ;
-- add a quiz to i_lesson
DELIMITER $$
CREATE PROCEDURE insert_quiz (IN i_name NVARCHAR(25), i_lesson INT) BEGIN
	INSERT INTO quiz
	(name, lesson) VALUES (i_name, i_lesson);
END $$
DELIMITER ;
-- add an article to i_lesson
DELIMITER $$
CREATE PROCEDURE insert_article (IN i_name NVARCHAR(25), i_lesson INT) BEGIN
	INSERT INTO article
	(name, lesson) VALUES (i_name, i_lesson);
END $$
DELIMITER ;
-- associate i_lesson with i_subject
DELIMITER $$
CREATE PROCEDURE add_subject_to_lesson (IN i_lesson INT, i_subject INT) BEGIN
	INSERT INTO subject_set
	(lesson, subject) VALUES (i_lesson, i_subject);
END $$
DELIMITER ;
-- add a section to i_article
DELIMITER $$
CREATE PROCEDURE insert_section (IN i_name NVARCHAR(25), i_article INT) BEGIN
	INSERT INTO section
	(name, article) VALUES (i_name, i_article);
END $$
DELIMITER ;
-- add a text element to i_section
DELIMITER $$
CREATE PROCEDURE insert_text
(IN i_data NVARCHAR(255), i_section INT)
BEGIN
	INSERT INTO element
	(section) VALUES (i_section);
	INSERT INTO text
	(id, data) VALUES (LAST_INSERT_ID(), i_data);
END $$
DELIMITER ;
-- add a picture element to i_section
DELIMITER $$
CREATE PROCEDURE insert_picture
(IN i_data BLOB, i_section INT)
BEGIN
	INSERT INTO element
	(section) VALUES (i_section);
	INSERT INTO picture
	(id, data) VALUES (LAST_INSERT_ID(), i_data);
END $$
DELIMITER ;
-- add a video element to i_section
DELIMITER $$
CREATE PROCEDURE insert_video
(IN i_data BLOB, i_section INT)
BEGIN
	INSERT INTO element
	(section) VALUES (i_section);
	INSERT INTO video
	(id, data) VALUES (LAST_INSERT_ID(), i_data);
END $$
DELIMITER ;
-- add a fill-in-the-blank question to i_quiz
DELIMITER 8==D
CREATE PROCEDURE insert_fill_blank
(IN i_data NVARCHAR(255), i_regex VARCHAR(255), i_quiz INT)
BEGIN
	INSERT INTO question
	(data, quiz) VALUES (i_data, i_quiz);
	INSERT INTO fill_blank
	(id, regex) VALUES (LAST_INSERT_ID(), i_regex);
END 8==D
DELIMITER ;
-- add a multiple-choice question to i_quiz
DELIMITER $$
CREATE PROCEDURE insert_multi_choice
(IN i_data NVARCHAR(255), i_quiz INT)
BEGIN
	INSERT INTO question
	(data, quiz) VALUES (i_data, i_quiz);
	INSERT INTO multi_choice
	(id) VALUES (LAST_INSERT_ID());
END $$
DELIMITER ;
-- add a choice to i_question
DELIMITER $$
CREATE PROCEDURE add_choice_to_question
(IN i_data NVARCHAR(25), i_question INT)
BEGIN
	INSERT INTO choice
	(data) VALUES (i_data);
	INSERT INTO choice_set
	(choice, question) VALUES (LAST_INSERT_ID(), i_question);
END $$
DELIMITER ;
-- add i_choice to a correct set of choices for i_question
DELIMITER $$
CREATE PROCEDURE add_choice_to_correct_set
(IN i_choice INT, i_question INT, i_set_id INT)
BEGIN
	INSERT INTO correct_set
	(choice, question, id)
	VALUES (i_choice, i_question, i_set_id);
END $$
DELIMITER ;
-- add i_usr score for i_quiz
DELIMITER $$
CREATE PROCEDURE add_score
(IN i_usr INT, i_quiz INT, i_val INT)
BEGIN
	INSERT INTO score
	(usr, quiz, val)
	VALUES
	(i_usr, i_quiz, i_val);
END $$
DELIMITER ;
-- update i_usr score for i_quiz
DELIMITER $$
CREATE PROCEDURE update_score
(IN i_usr INT, i_quiz INT, i_val INT)
BEGIN
	UPDATE score
	SET val = i_val
	WHERE usr = i_usr
	AND quiz = i_quiz;
END $$
DELIMITER ;
