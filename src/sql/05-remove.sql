USE course_builder;
DELIMITER $$
-- delete the given user
-- will cascade to:
-- producer, admin, score
CREATE PROCEDURE delete_user
(IN i INT) BEGIN
	DELETE FROM usr
	WHERE id = i;
END $$
-- delete the given producer
-- will cascade to:
-- lesson
CREATE PROCEDURE delete_producer
(IN i INT) BEGIN
	DELETE FROM producer
	WHERE id = i;
END $$
-- delete the given admin
-- will cascade to:
-- unplublish anything by this admin
CREATE PROCEDURE delete_admin
(IN i INT) BEGIN
	DELETE FROM admn
	WHERE id = i;
END $$
-- delete the given lesson
-- will cascade to:
-- quiz, article
CREATE PROCEDURE delete_lesson
(IN i INT) BEGIN
	DELETE FROM lesson
	WHERE id = i;
END $$
-- delete the given quiz
-- will cascade to:
-- question, score
CREATE PROCEDURE delete_quiz
(IN i INT) BEGIN
	DELETE FROM quiz
	WHERE id = i;
END $$
-- delete the given article
-- will cascade to:
-- section
CREATE PROCEDURE delete_article
(IN i INT) BEGIN
	DELETE FROM article
	WHERE id = i;
END $$
-- delete the given section
-- will cascade to:
-- element
CREATE PROCEDURE delete_section
(IN i INT) BEGIN
	DELETE FROM section
	WHERE id = i;
END $$
-- delete the given element
-- will cascade to:
-- picture, text video
CREATE PROCEDURE delete_element
(IN i INT) BEGIN
	DELETE FROM element
	WHERE id = i;
END $$
-- delete the given question
-- will cascade to:
-- multi_choice, fill_blank
CREATE PROCEDURE delete_question
(IN i INT) BEGIN
	DELETE FROM question
	WHERE id = i;
END $$
-- delete the given choice
-- will cascade to:
-- correct_set, common_choice, choice_set
CREATE PROCEDURE delete_choice
(IN i INT) BEGIN
	DELETE FROM choice
	WHERE id = i;
END $$
DELIMITER ;
