USE course_builder;
-- users
CREATE TABLE usr (
	id INT AUTO_INCREMENT,
	hash VARCHAR(40) NOT NULL,
	salt VARCHAR(25) NOT NULL,
	email VARCHAR(90) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (email)
);
CREATE TABLE producer (
	id INT,
	PRIMARY KEY (id),
	FOREIGN KEY (id) REFERENCES usr (id)
		ON DELETE CASCADE -- user deleted, so no producer
);
CREATE TABLE admn (
	id INT,
	PRIMARY KEY (id),
	FOREIGN KEY (id) REFERENCES usr (id)
		ON DELETE CASCADE -- user deleted, so no admin
);
-- lessons and their components
CREATE TABLE lesson (
	id INT AUTO_INCREMENT,
	producer INT NOT NULL,
	admn INT,
	name NVARCHAR(25) NOT NULL,
	FOREIGN KEY (producer) REFERENCES producer (id)
		ON DELETE CASCADE, -- lesson writer deleted, so no lesson
	FOREIGN KEY (admn) REFERENCES admn (id)
		ON DELETE SET NULL, -- unpublish if publisher deleted
	PRIMARY KEY (id)
);
CREATE TABLE quiz (
	id INT AUTO_INCREMENT,
	lesson INT NOT NULL,
	FOREIGN KEY (lesson) REFERENCES lesson (id)
		ON DELETE CASCADE, -- lesson deleted, so no quiz
	name NVARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE article (
	id INT AUTO_INCREMENT,
	lesson INT NOT NULL,
	FOREIGN KEY (lesson) REFERENCES lesson (id)
		ON DELETE CASCADE, -- lesson deleted, so no article
	name NVARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE subject (
	id INT AUTO_INCREMENT,
	name NVARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE subject_set (
	lesson INT,
	subject INT,
	FOREIGN KEY (lesson) REFERENCES lesson (id)
		ON DELETE CASCADE, -- lesson deleted, so no subject
	FOREIGN KEY (subject) REFERENCES subject (id)
		ON DELETE CASCADE, -- subject deleted, so no subject
	CONSTRAINT pk PRIMARY KEY (lesson, subject)
);
-- user relationships with lessons and quizes
CREATE TABLE score (
	usr INT,
	quiz INT,
	val INT NOT NULL,
	FOREIGN KEY (usr) REFERENCES usr (id)
		ON DELETE CASCADE, -- user deleted, so no score
	FOREIGN KEY (quiz) REFERENCES quiz (id)
		ON DELETE CASCADE, -- quiz deleted, so no score
	CONSTRAINT pk PRIMARY KEY (usr, quiz)
);
-- article components
CREATE TABLE section (
	id INT AUTO_INCREMENT,
	article INT NOT NULL,
	name NVARCHAR(25) NOT NULL,
	FOREIGN KEY (article) REFERENCES article (id)
		ON DELETE CASCADE, -- article deleted, so no section
	PRIMARY KEY (id)
);
CREATE TABLE element (
	id INT AUTO_INCREMENT,
	section INT NOT NULL,
	FOREIGN KEY (section) REFERENCES section (id)
		ON DELETE CASCADE, -- section deleted, so no element
	PRIMARY KEY (id)
);
CREATE TABLE text (
	id INT,
	data NVARCHAR(255) NOT NULL,
	FOREIGN KEY (id) REFERENCES element (id)
		ON DELETE CASCADE, -- element deleted, so no data
	PRIMARY KEY (id)
);
CREATE TABLE picture (
	id INT,
	data BLOB NOT NULL,
	FOREIGN KEY (id) REFERENCES element (id)
		ON DELETE CASCADE, -- element deleted, so no data
	PRIMARY KEY (id)
);
CREATE TABLE video (
	id INT,
	data BLOB NOT NULL,
	FOREIGN KEY (id) REFERENCES element (id)
		ON DELETE CASCADE, -- element deleted, so no data
	PRIMARY KEY (id)
);
-- quiz components
CREATE TABLE question (
	id INT AUTO_INCREMENT,
	quiz INT NOT NULL,
	data NVARCHAR(255) NOT NULL,
	FOREIGN KEY (quiz) REFERENCES quiz (id)
		ON DELETE CASCADE, -- quiz deleted, so no question
	PRIMARY KEY (id)
);
CREATE TABLE fill_blank (
	id INT,
	regex VARCHAR(255) NOT NULL,
	FOREIGN KEY (id) REFERENCES question (id)
		ON DELETE CASCADE, -- question deleted, so no blank
	PRIMARY KEY (id)
);
CREATE TABLE multi_choice (
	id INT,
	FOREIGN KEY (id) REFERENCES question (id)
		ON DELETE CASCADE, -- question deleted, so no multi_choice
	PRIMARY KEY (id)
);
CREATE TABLE choice (
	id INT AUTO_INCREMENT,
	data NVARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE common_choice (
	id INT,
	FOREIGN KEY (id) REFERENCES choice (id)
		ON DELETE CASCADE, -- choice deleted, so not common
	PRIMARY KEY (id)
);
INSERT INTO choice
(data)
VALUES
("true"),
("false");
INSERT INTO common_choice SELECT id FROM choice;
CREATE TABLE choice_set (
	choice INT,
	question INT,
	FOREIGN KEY (question) REFERENCES multi_choice (id)
		ON DELETE CASCADE, -- question deleted, so no choices
	FOREIGN KEY (choice) REFERENCES choice (id)
		ON DELETE CASCADE, -- choice deleted, so not a choice
	CONSTRAINT pk PRIMARY KEY (choice, question)
);
CREATE TABLE correct_set (
	choice INT,
	question INT,
	id INT NOT NULL,
	FOREIGN KEY (choice) REFERENCES choice (id)
		ON DELETE CASCADE, -- choice deleted, so not correct
	FOREIGN KEY (question) REFERENCES multi_choice (id)
		ON DELETE CASCADE, -- question deleted, so no correct choices
	CONSTRAINT pk PRIMARY KEY (choice, question, id)
);
