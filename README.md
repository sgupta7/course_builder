installing
----------

Some bash scripts are included to help with building and testing. `build.sh`
will run the MySQL scripts and copy other files to the necessary directories.
`clean.sh` will undo everything that `build.sh` did. `test.sh` will run a
series of test scripts located in the `test` directory.

Ensure that MySQL server is running, and that the executable is added to the
PATH system variable.
