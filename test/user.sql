USE course_builder;

START TRANSACTION;

SELECT "create and login valid user" as "";

CALL user_create("keypassa", "muhspamtrap@gmail.com");
CALL user_login("keypassa", "muhspamtrap@gmail.com");

SELECT "create additional valid user" as "";

CALL user_create("password123", "foo@bar.baz");
CALL user_login("password123", "foo@bar.baz");

SELECT "login with incorrect password for given email" as "";

CALL user_login("password124", "foo@bar.baz");

SELECT "login with correct password for different email" as "";

CALL user_login("password123", "muhspamtrap@gmail.com");

SELECT "make user admn" as "";

CALL insert_producer(LAST_INSERT_ID());
CALL user_login("password123", "foo@bar.baz");

ROLLBACK;
