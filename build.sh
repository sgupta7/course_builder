#!/bin/bash

SQLDIR=src/sql/*.sql
SQLSERV=mysql
SQLFLAG=

./clean.sh

echo "building database"

for f in $SQLDIR
do
	echo "executing: $f"
	cat $f | $SQLSERV $SQLFLAG
done
